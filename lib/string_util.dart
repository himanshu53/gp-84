class StringUtil {
  static const String gongImageUrl = "https://gong-staging.uc.r.appspot.com/44640a6400fd304092fa.png";
  static const String log_In = "Log In";
  static const String email = "Email";
  static const String password = "Password";
  static const String rememberMe = "Remember me";
  static const String forgotPassword = "Forgot Password?";
  static const String sendPassword = "Send Password?";
  static const String enterEmailBelow = "Enter your email below to receive your password reset instructions";

}