import 'package:gp84/string_util.dart';
import 'package:flutter/material.dart';

import 'forgot_password_screen.dart';
import 'local_auth_api.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {


  bool rememberMe = false;
  bool hasFingerPrint = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkDevideHasBioMetric();
  }

  checkDevideHasBioMetric()async{
    final isAuthenticate = await LocalAuthApi.hasBiometric();
    if(isAuthenticate){
      setState(() {
        hasFingerPrint = true;
      });
    }else{
      setState(() {
        hasFingerPrint = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var height=  MediaQuery.of(context).size.height;
    var width=  MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xffdc3545),
      appBar: AppBar(
        title: Text(StringUtil.log_In),
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.white,), onPressed: () {  },),
        centerTitle: true,
        backgroundColor: Color(0xffdc3545),
      ),
      body: SafeArea(
        child: Container(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 16),
            children: [
              SizedBox(height: height*0.08),
              Center(
                child: SizedBox(
                    height: 150,
                    width: 200,
                    child: Image.network(StringUtil.gongImageUrl, fit: BoxFit.fill)),
              ),
              SizedBox(height: height*0.04),

              Text(StringUtil.email, style: TextStyle(
                color: Colors.white
              ),),
              SizedBox(height: height*0.01),
              Container(
                height: 40,
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  cursorColor: Colors.white,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(bottom: 10),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(12)
                    ),
                    prefixIcon: Container(
                      width: 30,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12),
                          bottomLeft: Radius.circular(12),
                        )
                      ),
                      child: Center(child: Icon(Icons.email, color: Color(0xffdc3545)),),
                    )
                  ),
                ),
              ),

              SizedBox(height: height*0.02),

              Text(StringUtil.password, style: TextStyle(
                  color: Colors.white
              ),),
              SizedBox(height: height*0.01),
              Container(
                height: 40,
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  cursorColor: Colors.white,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 10),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(12)
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(12)
                      ),
                      prefixIcon: Container(
                        width: 30,
                        margin: EdgeInsets.only(right: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              bottomLeft: Radius.circular(12),
                            )
                        ),
                        child: Center(child: Icon(Icons.vpn_key_rounded, color: Color(0xffdc3545)),),
                      )
                  ),
                ),
              ),

              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Row(
                      children: [
                        Theme(
                          data: ThemeData(
                           checkboxTheme:CheckboxThemeData(
                             fillColor: MaterialStateColor.resolveWith(
                                   (states) {
                                 if (states.contains(MaterialState.selected)) {
                                   return Color(0xffdc3545); // the color when checkbox is selected;
                                 }
                                 return Colors.white; //the color when checkbox is unselected;
                               },
                             ),
                           ),
                          ),
                          child: Checkbox(
                              value: rememberMe, onChanged: (value){
                            setState(() {
                              rememberMe = value!;
                            });
                          }),
                        ),
                        //SizedBox(width: 6),
                        Text(StringUtil.rememberMe, style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                  Expanded(child: Text(StringUtil.forgotPassword, style: TextStyle(color: Colors.white))),
                ],
              ),
               SizedBox(height: height*0.04),

              Row(
                children: [
                  Expanded(
                    child: RaisedButton(
                        onPressed: (){},
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        color: Colors.orange,
                      child: Text(StringUtil.log_In,  style: TextStyle(color: Colors.white))),
                  ),
                  SizedBox(width: 10,),
                  hasFingerPrint==true? InkWell(
                    onTap: ()async{
                      final isAuthenticate = await LocalAuthApi.authenticate();
                      if(isAuthenticate){
                        Navigator.push(context, MaterialPageRoute(builder: (_)=>ForgotPasswordScreen()));
                      }
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        color: Colors.orange,
                        shape: BoxShape.circle
                      ),
                      child: Icon(Icons.fingerprint),
                    ),
                  ):Container()
                ],
              ),

              SizedBox(height: height*0.04),

              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Don't have an account? ",
                      style: TextStyle(color: Colors.white)
                    ),
                    TextSpan(
                        text: "Sign Up",
                        style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold)
                    ),
                  ]
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
