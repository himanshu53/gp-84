import 'package:gp84/string_util.dart';
import 'package:flutter/material.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    var height=  MediaQuery.of(context).size.height;
    var width=  MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xffdc3545),
      appBar: AppBar(
        title: Text(StringUtil.forgotPassword),
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.white,), onPressed: () {  },),
        centerTitle: true,
        backgroundColor: Color(0xffdc3545),
      ),
      body: SafeArea(
        child: Container(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 16),
            children: [
              SizedBox(height: height*0.08),
              Center(
                child: SizedBox(
                    height: 150,
                    width: 200,
                    child: Image.network(StringUtil.gongImageUrl, fit: BoxFit.fill)),
              ),
              SizedBox(height: height*0.04),
              Text(StringUtil.enterEmailBelow,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17
                ),),
              SizedBox(height: height*0.04),

              Text(StringUtil.email, style: TextStyle(
                  color: Colors.white
              ),),
              SizedBox(height: height*0.01),
              Container(
                height: 40,
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  cursorColor: Colors.white,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 10),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(12)
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(12)
                      ),
                      prefixIcon: Container(
                        width: 30,
                        margin: EdgeInsets.only(right: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12),
                              bottomLeft: Radius.circular(12),
                            )
                        ),
                        child: Center(child: Icon(Icons.email, color: Color(0xffdc3545)),),
                      )
                  ),
                ),
              ),

              SizedBox(height: height*0.04),

              RaisedButton(
                  onPressed: (){},
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  color: Colors.orange,
                  child: Text("Send Password",  style: TextStyle(color: Colors.white))),

              SizedBox(height: height*0.04),
            ],
          ),
        ),
      ),
    );
  }
}
